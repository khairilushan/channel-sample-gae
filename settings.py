from djangoappengine.settings_base import *

import os

DEBUG = False
TEMPLATE_DEBUG = DEBUG

SECRET_KEY = 'h^*8i&wtn2n4f7!vq788)9qu$=yoja5($ei+ptbke6gehhz!q_6d'

INSTALLED_APPS = (
    'djangoappengine',
    'djangotoolbox',
    'django.contrib.auth',
    'django.contrib.sessions',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'chat',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.request',
    'django.core.context_processors.media',
)


DEFAULT_CHARSET = 'utf-8'
FILE_CHARSET = 'utf-8'

LOGIN_REDIRECT_URL = '/'
LOGIN_URL = '/login/'

TIME_ZONE = 'America/Chicago'
LANGUAGE_CODE = 'en-us'

ADMIN_MEDIA_PREFIX = '/media/admin/'
MEDIA_ROOT = ''
MEDIA_URL = '/media/'
TEMPLATE_DIRS = (os.path.join(os.path.dirname(__file__), 'templates'))

ROOT_URLCONF = 'urls'
