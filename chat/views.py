# -*- coding: utf-8 -*-
# 
# realtime chat sample using channel API Google App Engine
#
# coder: 5ynL0rd http://void-labs.appspot.com

from django.views.generic.simple import direct_to_template
from django.shortcuts import get_object_or_404, render_to_response
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.core.urlresolvers import reverse

from chat.models import ChannelStorage
from google.appengine.api import channel
from datetime import datetime

import simplejson
import uuid

def channel_test(request):
    ip = request.META.get('REMOTE_ADDR', 'unknown')
    ip = '.'.join(ip.split('.')[0:2])+'.xxx.xxx'
    ua = request.META.get('HTTP_USER_AGENT', 'unknown')
    channel_id = request.POST.get('channel_id')                
    if request.POST.get('msg'):
        for i in ChannelStorage.objects.all():
            delta = datetime.utcnow() - i.date
            if delta.seconds > 7000:
                msg = {'data': '%s: <font color="red">TIMEOUT!</font>' %ip, 'key': channel_id}
                msg = simplejson.dumps(msg)
                channel.send_message(i.channel_id, msg)  
                ChannelStorage.objects.get(channel_id=i.channel_id).delete()
            else:
                msg = {'data': '%s: <b>%s<b>' %(ip, request.POST.get('msg').replace('<','&lt;')), 'key': channel_id}
                msg = simplejson.dumps(msg)
                channel.send_message(i.channel_id, msg)
        return HttpResponse('%s: <b>%s</b>' %(ip, request.POST.get('msg').replace('<','&lt;')))
    if request.POST.get('notif'):
        for i in ChannelStorage.objects.all():
            delta = datetime.utcnow() - i.date
            if delta.seconds > 7000:
                msg = {'data': '%s: <font color="red">TIMEOUT!</font>' %ip, 'key': channel_id}
                msg = simplejson.dumps(msg)
                channel.send_message(i.channel_id, msg)  
                ChannelStorage.objects.get(channel_id=i.channel_id).delete()
            else:
                msg = {'data': '%s: <font color="green">%s</font>' %(ip, request.POST.get('notif')), 'key': channel_id}
                msg = simplejson.dumps(msg)
                channel.send_message(i.channel_id, msg)
        return HttpResponse('%s: <font color="green">%s</font>' %(ip, request.POST.get('notif')))
    return HttpResponse()

def main(request):
    channel_id = str(uuid.uuid1())
    token = channel.create_channel(channel_id)
    new_channel = ChannelStorage.objects.create(channel_id=channel_id, date=datetime.utcnow())
    new_channel.save()
    return render_to_response('main.html', locals(), context_instance=RequestContext(request))
