from google.appengine.ext import webapp

class MainPage(webapp.RequestHandler):
    def get(self):
        print self.request.get('from')
        self.response.headers['Content-Type'] = 'text/plain'
        self.response.out.write('Hello, webapp World!')

    def post(self):
        print self.request
        self.response.headers['Content-Type'] = 'text/plain'
        self.response.out.write('Hello, webapp World!')

app = webapp.WSGIApplication([('/', MainPage)])
