# -*- coding: utf-8 -*-
from django.db import models

class ChannelStorage(models.Model):
    channel_id = models.CharField(max_length=64)
    date = models.DateTimeField(null=True)
    def __unicode__(self):
        return self.channel_id
